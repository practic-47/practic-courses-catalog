import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.micrometer.core.instrument.Metrics
import io.micrometer.prometheus.PrometheusConfig
import io.micrometer.prometheus.PrometheusMeterRegistry

fun main(args: Array<String>) {
    JavalinApp(7000, HashSet()).init()
}

class JavalinApp(
        private val port: Int,
        private val courses: MutableCollection<String>
) {
    private val pageSize: Int = 10

    fun init(): Javalin {
        val app = Javalin.create().apply {
            port(port)
            exception(Exception::class.java) { e, _ -> e.printStackTrace() }
	    enableMicrometer()
        }.start()
        val registry = PrometheusMeterRegistry(PrometheusConfig.DEFAULT)
	Metrics.globalRegistry.add(registry)
        app.routes {
            path("api") {
                path("courses") {
                    get { ctx -> ctx.json(courses.take(pageSize)) }
                }
            }
	    get("/prometheus") {
	    	ctx -> ctx.result(registry.scrape())
	    }
        }
        return app
    }
}
