Feature: Query

    Scenario Outline: a few courses
        Given System is working on <port>
        Given System has <total> courses in catalog
        When Guest request list of courses
        Then System returns <pages> page of courses with <received> courses in it

    Examples:
        | port | total | pages | received |
        | 7000 | 47    | 5     | 10       |
        | 7000 | 5     | 1     | 5        |
        | 7000 | 0     | 1     | 0        |