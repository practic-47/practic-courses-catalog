import cucumber.api.java8.En
import org.junit.Assert

/**
 * Step definitions for `Query` feature.
 *
 * @author Nikita_Puzankov
 */
class QueryStepDefs(val testInstance: TestInstance) : En {
    private val pageSize: Int = 10
    private val courses: MutableCollection<String> = HashSet()
    private val result: MutableCollection<String> = HashSet()
    private val url = "http://localhost:7000/"

    init {
        Given("^System is working on (\\d+)$") { port: Int ->
            testInstance.app =
                    JavalinApp(port, courses).init()
        }
        Given("^System has (\\d+) courses in catalog$") { coursesSize: Int ->
            for (i in 1..coursesSize) {
                courses.add(i.toString())
            }
            testInstance.courses = this.courses
        }
        When("^Guest request list of courses$") {
            result.addAll(
                    khttp.get(url = this.url + "api/courses").jsonArray.map { any -> any.toString() }
            )
        }
        Then("^System returns (\\d+) page of courses with (\\d+) courses in it$") { pagesCount: Int, expectedSize: Int ->
            Assert.assertEquals(pagesCount, (testInstance.courses.size / pageSize).plus(1))
            Assert.assertEquals(expectedSize, result.size)
            testInstance.app.stop()
        }
    }
}