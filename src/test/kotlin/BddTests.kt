import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import org.junit.runner.RunWith

/**
 * Class to run BDD tests.
 *
 * @author Nikita_Puzankov
 */
@RunWith(Cucumber::class)
@CucumberOptions(plugin = ["pretty"])
class RunAllTests
