import io.javalin.Javalin

/**
 * `World` which contains information shared in scope of test scenario.
 *
 * @author Nikita_Puzankov
 */
class TestInstance {
    lateinit var app: Javalin
    lateinit var courses: MutableCollection<String>
}