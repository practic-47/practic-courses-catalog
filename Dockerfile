FROM birdy/graalvm:latest

ADD target/practic-courses-catalog-1.0-SNAPSHOT-jar-with-dependencies.jar /tmp/app.jar
ADD reflection.json /tmp/reflection.json
RUN cd /tmp && native-image -jar app.jar -H:ReflectionConfigurationFiles=reflection.json -H:+JNI \
  -H:Name=practic-courses --static --delay-class-initialization-to-runtime=io.javalin.json.JavalinJson

FROM scratch
COPY --from=0 /tmp/practic-courses /
ENTRYPOINT ["/practic-courses"]