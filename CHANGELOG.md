# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2019-05-25
### Added
- Initial maven structure
- CHANGELOG
- Javalin web application
- List of courses API
- Cucumber scenarious with automated tests for `Query` feature
- Assembly configuration
- Prometheus monitoring endpoint via micrometer
### Changed
### Deprecated 
### Removed
### Fixed 
### Security