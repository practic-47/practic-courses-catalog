# practic-courses-catalog

Courses catalogue application provides a `CQRS` architecture based system for managing courses as administrator and receiving information as a client.


## Build

```
mvn clean install
```

## Depoly

```
docker build -t humb1t/practic-course-catalog:1.0 .
```

## Start

```
docker run -p 127.0.0.1:7000:7000 humb1t/practic-course-catalog:1.0
```
